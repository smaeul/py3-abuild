# Copyright (c) 2018 Max Rees
# Distributed under GPL-2.0-only
# See LICENSE for more information.
import os
import shlex
from glob import glob
from pathlib import Path

import abuild.config as conf
import abuild.common as com
import abuild.child as child

def up2date_bool(self):
    """
    .. method:: Abuild.up2date_bool()

       Check if the packages have already been built or not. This is the
       internal version that returns ``True`` or ``False`` instead of
       raising an exception.

       If ``-f`` was given, this always returns ``False``. If any of the
       packages (as listed by ``listpkg``) are missing, return ``False``.
       If any of the $source files are newer than the main package, return
       ``False``. If any of the non-remote ``source`` files are missing,
       return ``False``.

       :rtype: bool
    """
    if self.settings["force"]:
        return False

    srcs = set(self.source.copy())
    tail = f"-{self.pkgver}-r{self.pkgrel}.apk"

    for pkg in self.allpackages:
        name = pkg
        pkg = self.allpackages[pkg]

        if pkg.arch == "noarch":
            arch = self.env["CARCH"]
        else:
            arch = pkg.arch

        apk = self.repopath / arch / (name + tail)
        if name == self.pkgname:
            mainapk = apk

        if not apk.is_file():
            self.debug("Missing {apk}")
            return False

        apk_s = shlex.quote(str(apk))
        sha256 = child.get_stdout_strip(
            f"gunzip -c < {apk_s} | tar xOf - .APKBUILD.sha256 2>/dev/null",
            shell=True, retcodes=[1])
        if sha256 and sha256 != self.APKBUILD.sha256:
            self.debug("APKBUILD checksum changed")
            return False
        if not sha256:
            # We want to check the mtime on the APKBUILD... but only if there
            # was no embedded checksum in one of the APKs
            srcs.add("APKBUILD")

    mainapk_mtime = mainapk.stat().st_mtime

    for src in srcs:
        if com.is_remote(src):
            path = Path(self.env["SRCDEST"]) / com.filename_from_uri(src)
            is_remote = True
        else:
            path = Path(self.env["startdir"]) / src
            is_remote = False

        if not path.is_file():
            if is_remote:
                continue
            else:
                self.error("Could not find", str(path))
                return False

        if path.stat().st_mtime > mainapk_mtime:
            self.debug(f"{path.name} is newer than {mainapk.name}")
            return False

    return True

def up2date(self):
    """
    .. method:: Abuild.up2date()

       Compare source file and APK file modification times
    """
    if not self.up2date_bool():
        raise com.abuild_error()

def index(self):
    """
    .. method:: Abuild.index()

       Update and sign the $REPODEST/$repo repository index
    """
    com.set_default(
        self.env, "DESCRIPTION", (
            self.env["repo"] + " " + child.get_stdout_strip(
                self.env["GIT"] + " describe 2>/dev/null",
                shell=True, retcodes=[128])))

    for arch in self.allarches:
        self.msg(
            f"Updating the {self.env['repo']}/{arch}"
            " repository index...")
        if not self.repopath.is_dir():
            self.die(f"Could not find {self.repopath}")
        path = self.repopath / arch
        APKINDEX = path / "APKINDEX.tar.gz"
        APKINDEX_new = path / (f"APKINDEX.tar.gz.{os.getpid()}")

        # Generate new APKINDEX.tar.gz.$$ (unsigned)
        args = ["apk", "index", "--quiet"]
        if APKINDEX.is_file():
            args += ["--index", str(APKINDEX)]

        args += [
            "--output", str(APKINDEX_new), "--description",
            self.env["DESCRIPTION"], "--rewrite-arch", arch]
        args += glob(str(path / "*.apk"))
        child.get_retcode(args)

        # Sign and move APKINDEX.tar.gz.$$
        self.msg("Signing the index...")
        child.get_retcode(["abuild-sign", "-q", str(APKINDEX_new)])
        APKINDEX_new.chmod(0o644)
        APKINDEX_new.rename(APKINDEX)

def checksum(self):
    """
    .. method:: Abuild.checksum()

       Update the checksums in the APKBUILD

       All :data:`.CHECKSUMS_READ` properties will be removed, and new ones
       according to `.CHECKSUMS_WRITE` will be written.
       :meth:`.Abuild.fetch` will automatically be called.
    """
    self.fetch()
    os.chdir(self.env["srcdir"])

    for algo in conf.CHECKSUMS_READ:
        if algo in self.APKBUILD.meta:
            self.msg("Removing", algo, "from APKBUILD")
            child.get_retcode([
                "sed", "-i", "-e",
                f"/^{algo}=\"/,/\"$/d; /^{algo}=/,/$/d",
                str(self.file)])
            del self.APKBUILD.meta[algo]

    args = ["<algo cmd>"]
    args.extend([com.filename_from_uri(src) for src in self.source])

    with open(self.file, "a") as f:
        for algo in conf.CHECKSUMS_WRITE:
            self.msg("Updating", algo, "in APKBUILD...")
            args[0] = algo.rstrip("s")
            csums = child.get_stdout_strip(args)
            self.APKBUILD.meta[algo] = csums.split("\n")
            print("{}=\"{}\"".format(algo, csums), file=f)

    os.chdir(self.env["startdir"])

def listpkg(self):
    """
    .. method:: Abuild.listpkg()

       List target APK filenames for this APKBUILD
    """
    tail = "-{}-r{}.apk".format(self.pkgver, self.pkgrel)
    for pkg in self.allpackages:
        print(pkg + tail)

def cleanpkg(self):
    """
    .. method:: Abuild.cleanpkg()

       Remove all APKs for this package
    """
    # TODO: source packages?
    self.msg("Cleaning built packages...")
    tail = "-{}-r{}.apk".format(self.pkgver, self.pkgrel)
    head = self.repopath

    for pkg in self.allpackages:
        arch = self.allpackages[pkg].arch
        if arch == "noarch":
            arch = self.env["CARCH"]
        path = head / arch / (pkg + tail)
        com.safe_rm(path)

    self.index()

def cleanoldpkg(self):
    """
    .. method:: Abuild.cleanoldpkg()

       Remove all APKs except for the version defined in the APKBUILD
    """
    self.msg(
        "Cleaning all packages except"
        f" {self.pkgver}-r{self.pkgrel}...")
    head = self.repopath
    tail = "-[0-9]*.apk"
    tail2 = f"-{self.pkgver}-r{self.pkgrel}.apk"

    for pkg in self.allpackages:
        matches = glob(str(head / "*" / (pkg + tail)))
        for f in matches:
            f = Path(f)
            if f.name != f"{pkg}{tail2}":
                com.safe_rm(f)

    self.index()
