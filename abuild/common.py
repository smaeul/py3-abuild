# Copyright (c) 2018 Max Rees
# Distributed under GPL-2.0-only
# See LICENSE for more information.
"""
Module abuild.common
====================

.. module:: abuild.common
   :synopsis: Small, common functions used by the py3-abuild package.
"""
import sys        # stderr
import enum       # Flag
import logging
import math       # log2
from os import symlink, environ, readlink
from pathlib import Path
from fnmatch import fnmatchcase
from grp import getgrnam
from pwd import getpwnam
from shlex import quote as shell_quote

import abuild.config as conf
import abuild.child as child

class DAGOpts(enum.Flag):
    NO = 0
    YES = 1
    ONLY = 2
    DOT = ONLY | 4

class DepOpts(enum.Flag):
    NONE = 0
    IGNORE = 1
    INSTALL = 2
    BUILD = 4
    UPGRADE = BUILD | 8

class abuild_error(Exception):
    def __init__(self, message="", name="abuild"):
        """
        .. class:: abuild_error([message="", name="abuild"])

           This is a simple :exc:`Exception` subclass that is raised when a
           fatal error occurs. The :attr:`~.abuild_error.name` and
           :attr:`~.abuild_error.message` attributes are used to print pretty
           console messages.
        """
        self.name = name
        self.msg = message

        super().__init__()

def unset_or_null(env, name):
    """
    .. function:: unset_or_null(env, name)

       Returns True if ``name`` is either not in ``env``, or if ``env[name]``
       is falsy.

       :param mapping env: The mapping to check against.
       :param hashable name: The key to check for.
       :rtype: bool
    """
    return name not in env or not env[name]

def set_default(env, name, default):
    """
    .. function:: set_default(env, name, default)

       If name is either not in ``env``, or ``env[name]`` is falsy, perform
       ``env[name] = default``.

       :param mapping env: The mapping to check against.
       :param hashable name: The key to check for.
       :param default: The default value to assign to ``env[name]``.
    """
    if unset_or_null(env, name):
        env[name] = default

def debug(*args, name="abuild", **kwargs):
    """
    .. function:: debug(*args, [name="abuild", **kwargs])
    """
    message = " ".join([str(arg) for arg in args])
    logging.getLogger(name).debug(message, **kwargs)

def msg(*args, name="abuild", **kwargs):
    """
    .. function:: msg(*args, [name="abuild", **kwargs])
    """
    message = " ".join([str(arg) for arg in args])
    logging.getLogger(name).info(message, **kwargs)

def msg2(*args, name="abuild", **kwargs):
    """
    .. function:: msg2(*args, [name="abuild", **kwargs])
    """
    message = " ".join([str(arg) for arg in args])
    logging.getLogger(name).log(25, message, **kwargs)

def warning(*args, name="abuild", **kwargs):
    """
    .. function:: warning(*args, [name="abuild", **kwargs])
    """
    message = " ".join([str(arg) for arg in args])
    logging.getLogger(name).warning(message, **kwargs)

def error(*args, name="abuild", **kwargs):
    """
    .. function:: error(*args, [name="abuild", **kwargs])

       These are convenience functions that pass the specified log message to
       the appropriate logger (as determined by ``name``). ``*args`` are
       concatenated into a single string and passed to the appropriate
       :class:`logging.Logger` method along with ``**kwargs``. Note that
       :func:`.msg` corresponds to a log level of ``INFO``.
    """
    message = " ".join([str(arg) for arg in args])
    logging.getLogger(name).error(message, **kwargs)

def die(*args, name="abuild", **kwargs):
    """
    .. function:: die(*args, [name="abuild", **kwargs])

       Same as above, but log a critical error and raise an
       :exc:`.abuild_error` exception.
    """
    message = " ".join([str(arg) for arg in args])
    logging.getLogger(name).error(message, **kwargs)
    raise abuild_error(message, name=name)

def safe_rm(f):
    """
    .. function:: safe_rm(f)

       Similar to ``rm -f``: ignores whether the file exists or not. This is a
       wrapper for :meth:`pathlib.Path.unlink` that discards
       :exc:`FileNotFoundError`.

       :param f: The filename to remove.
       :type f: str or :class:`pathlib.Path`
    """
    if not isinstance(f, Path):
        f = Path(f)
    f.resolve()
    try:
        debug("Removing", f)
        f.unlink()
    except FileNotFoundError:
        pass

def force_symlink(target, link_name):
    """
    .. function:: force_symlink(target, link_name)

       Similar to ``ln -sf``: creates a symbolic link named ``link_name`` that
       points to ``target``, overwriting ``link_name`` if it already exists.
       This is a wrapper for :func:`os.symlink` that discards
       :exc:`FileExistsError`.

       :param target: The path that the symlink should point to.
       :type target: str or :class:`pathlib.Path`
       :param link_name: The name to give the symlink.
       :type link_name: str or :class:`pathlib.Path`
    """
    try:
        debug("Symlinking", link_name, "to", target)
        symlink(target, link_name)
    except FileExistsError:
        pass

def glob_match(needle, haystack):
    """
    .. function:: glob_match(needle, haystack) str:

       Check if ``needle`` matches against any of the globs in ``haystack``.
       This function uses :func:`fnmatch.fnmatchcase` to perform the globbing.

       :param str needle: The string that should be checked.
       :param iterable haystack: The list of glob patterns to match against.
       :rtype: bool

       >>> glob_match("example.tar.gz", ("*.tar.gz", "*.tgz"))
       True
       >>> glob_match("example.tar.bz2", ("*.tar.gz", "*.tgz"))
       False
    """
    for pattern in haystack:
        if fnmatchcase(needle, pattern):
            return True
    return False

def glob_switch(needle, haystack):
    """
    .. function:: glob_switch(needle, haystack)

       If ``needle`` matches any of the pattern keys in ``haystack``, return
       ``haystack[pattern]``. This function uses :func:`fnmatch.fnmatchcase` to
       perform the globbing.

       .. todo:: Make the no-matches return value configurable.

       :param str needle: The string that should be checked.
       :param mapping haystack:
          The mapping of glob patterns to values to match against.
       :returns:

          The corresponding value for the key ``pattern`` in ``haystack``,
          if ``pattern`` matches ``needle``. If there are no matches, return
          the string ``"unknown"``.

       >>> glob_switch("example.tgz", {"*.tgz": "gunzip", "*.tbz2": "bunzip2"})
       'gunzip'
       >>> glob_switch("example.txz", {"*.tgz": "gunzip", "*.tbz2": "bunzip2"})
       'unknown'
    """
    for pattern in haystack:
        if fnmatchcase(needle, pattern):
            return haystack[pattern]
    return "unknown"

def arch_to_hostspec(arch):
    """
    .. function:: arch_to_hostspec(arch)

       This function returns the corresponding ``hostspec`` for ``arch`` as
       defined by :data:`.ARCH_TO_HOSTSPEC`, or the string ``"unknown"`` if
       there is no corresponding ``hostspec``.
    """
    if arch in conf.ARCH_TO_HOSTSPEC:
        return conf.ARCH_TO_HOSTSPEC[arch]

    return "unknown"

def hostspec_to_arch(hostspec):
    """
    .. function:: hostspec_to_arch(hostspec)

       This function returns the corresponding ``arch`` for ``hostspec`` as
       defined by the :data:`.HOSTSPEC_TO_ARCH` glob mapping.
    """
    return glob_switch(hostspec, conf.HOSTSPEC_TO_ARCH)

def hostspec_to_libc(hostspec):
    """
    .. function:: hostspec_to_libc(hostspec)

       Return the corresponding ``libc`` string for ``hostspec`` as defined by
       the :data:`.HOSTSPEC_TO_LIBC` glob mapping.
    """
    return glob_switch(hostspec, conf.HOSTSPEC_TO_LIBC)

def get_remote_uri(src):
    """
    .. function:: get_remote_uri(src)

       Return the URI part of a remote source.

       :rtype: str
    """
    i = src.find("::")
    if i != -1:
        src = src[i + 2 : ]
    return src

def is_remote(src):
    """
    .. function:: is_remote(src)

       Return ``True`` if the source is remote, ``False`` otherwise.

       :rtype: bool
       .. todo:: Make the prefixes that denote a remote source configurable.

       >>> is_remote("example.patch")
       False
       >>> is_remote("https://example.com/example.tar.gz")
       True
       >>> is_remote("example-1.0-r0.tar.gz::https://example.com/example.tar.gz")
       True
    """
    i = src.find("::")
    if i != -1:
        src = src[i + 2 : ]
    return src.startswith(("https://", "http://", "ftp://"))

def filename_from_uri(src):
    """
    .. function:: filename_from_uri(src)

       Return the base filename from a given source. For remote sources,
       this is either the part after the last slash or the component before
       the "::" prefix. For local sources, this is simply the filename.

       :param str src: The ``source`` specification to analyze.
       :rtype: str

       >>> filename_from_uri("example.patch")
       'example.patch'
       >>> filename_from_uri("https://example.com/example.tar.gz")
       'example.tar.gz'
       >>> filename_from_uri("example-1.0-r0.tar.gz::https://example.com/example.tar.gz")
       'example-1.0-r0.tar.gz'
    """
    i = src.find("::")
    if i == -1:
        i = src.rfind("/")
        # No bounds check is necessary since -1 and len(src) - 1 both work
        # below. Note that if i == len(src) - 1, then this will become an empty
        # string.
        src = src[i + 1 : ]
    else:
        src = src[ : i]

    return src

def atomize(atom):
    """
    .. function:: atomize(atom)

       Remove version qualifiers from a package atom.

       :param str atom: The package atom to clean.
       :rtype: str

       >>> atomize("pkg>0.0.1")
       'pkg'
       >>> atomize("pkg<=0.0.1")
       'pkg'
    """
    pos = atom.find(">")
    if pos == -1:
        pos = atom.find("<")
        if pos == -1:
            pos = atom.find("=")
            if pos == -1:
                pos = atom.find("~")
                if pos == -1:
                    return atom

    return atom[0:pos]

def safe_getgrnam(group):
    """
    .. function:: safe_getgrnam(group)

       This is a wrapper for :func:`grp.getgrnam` that returns ``False``
       instead of raising :exc:`KeyError`.

       :rtype: str or bool
    """
    try:
        return getgrnam(group)
    except KeyError:
        return False

def safe_getpwnam(user):
    """
    .. function:: safe_getpwnam(user)

       This is a wrapper for :func:`pwd.getpwnam` that returns ``False``
       instead of raising :exc:`KeyError`.

       :rtype: str or bool
    """
    try:
        return getpwnam(user)
    except KeyError:
        return False

def parse_trigger(trigger):
    """
    .. function:: parse_trigger(trigger)

       Split a ``triggers`` specification into the package name, filename, and
       directories. Raises an exception if the specification has no associated
       directories or filename extension.

       :rtype: tuple

       >>> parse_trigger("subpkg.trigger=/dir1/*:/dir2/*")
       ('subpkg', 'subpkg.trigger', '/dir1/*:/dir2/*')
    """
    i = trigger.find("=")
    if i == -1:
        die(f"Missing directory for triggers='{trigger}'")
    filename = trigger[0 : i]
    directories = trigger[i + 1 : ]
    i = filename.find(".")
    if i == -1:
        die(f"Missing filename extension for triggers='{trigger}'")
    pkgname = filename[0 : i]
    return (pkgname, filename, directories)

def resolve_symlink_to_root(symlink_path, root):
    """
    .. function:: resolve_symlink_to_root(symlink_path, root)

       Read the symbolic link located at ``symlink_path`` and normalize it with
       respect to ``root``. It is assumed that the symlink exists within
       ``root``. Returns the ``symlink_path`` less ``root``, the relative
       target of the symlink, and the resolved target of the symlink with
       respect to ``root``.

       :rtype: tuple
    """
    rel_path = str(symlink_path).replace(str(root) + "/", "./", 1)
    target = Path(readlink(symlink_path))

    if not target.is_absolute():
        abs_target = Path(rel_path).parent / target
    else:
        abs_target = target
    abs_target = str(abs_target).lstrip("/")

    return rel_path, target, Path(root) / abs_target

def parse_install(install):
    """
    .. function:: parse_install(install)

       Split a ``install`` specification into the package name and filename.
       Raises an exception if the specification has no filename extension.

       :rtype: tuple

       >>> parse_install("subpkg.pre-install")
       ('subpkg', 'subpkg.pre-install')
    """
    filename = install
    i = filename.find(".")
    if i == -1:
        die(f"Missing filename extension for install='{install}'")
    pkgname = filename[0 : i]
    return (pkgname, filename)

_SIZE_SUFFIXES = ("B", "KiB", "MiB", "GiB", "TiB", "PiB")
def human_readable_bytesize(size):
    """
    .. function:: human_readable_bytesize(size)

       Convert the given size in bytes to a human readable size.

       :param int size:
       :rtype: str

       >>> human_readable_bytesize(0)
       '0.0 B'
       >>> human_readable_bytesize(1023)
       '1023.0 B'
       >>> human_readable_bytesize(1024)
       '1.0 KiB'
       >>> human_readable_bytesize(154435553)
       '147.3 MiB'
       >>> human_readable_bytesize(6000000000)
       '5.6 GiB'
       >>> human_readable_bytesize(1.154E18)
       '1025.0 PiB'
    """
    if size == 0:
        i = 0
    else:
        i = min(int(math.log(size, 2 ** 10)), len(_SIZE_SUFFIXES) - 1)
        if i < 0:
            i = 0
        size /= 2 ** (10 * i)

    return f"{size:.1f} {_SIZE_SUFFIXES[i]}"

class abuildLogFormatter(logging.Formatter):
    def __init__(self, fmt=None, use_color=True, show_time=False, **kwargs):
        """
        .. class:: abuildLogFormatter([fmt=None, use_color=True, show_time=False, **kwargs])

           This is the log formatter used for all messages in the py3-abuild
           package. The ``fmt`` argument should probably be left alone because
           the default already handles ``use_color``, ``show_time``, and
           pretty prints both the log level and the logger name. See
           :meth:`.abuildLogFormatter.format` for more information.
        """
        #:    :INSERT:
        if not fmt:
            if show_time:
                fmt = "%(magenta)s%(asctime)s "
            else:
                fmt = ""
            fmt += "%(levelcolor)s%(prettylevel)s"
            fmt += "%(normal)s%(strong)s%(prompt)s%(normal)s%(message)s"
        #:    :INSERT:

        super().__init__(fmt, **kwargs)
        self.use_color = use_color

    def format(self, record):
        """
        .. method:: abuildLogFormatter.format(record)

           Format a log record. In addition to the usual ``%(message)s`` and
           ``%(asctime)s`` substitutions, this method also substitutes:

           ``%(levelcolor)s``
              The color for the logging level as defined by :data:`.COLORS`.
              This will be the empty string if ``use_colors`` is ``False``.
           ``%(prettylevel)s``
              The logging level plus a colon and a space.  If the logging
              level is ``INFO``, this is the empty string.
           ``%(prompt)s``
              The logger name without the ``abuild.`` prefix (e.g.
              ``pkgname``if the logger name is ``abuild.pkgname``) plus a colon
              and a space. If the prompt ends in an asterisk (denoting presence
              of a fakeroot environment) and ``use_colors`` is ``True``, the
              asterisk will be colored blue.
           ``%(normal)s``, ``%(strong)s``, etc
              The colors as defined in :mod:`.abuild.config`. These will be
              the empty string if ``use_colors`` is ``False``.
        """
        if self.use_color and record.levelname in conf.COLORS:
            record.levelcolor = conf.COLORS[record.levelname]
            record.strong = conf.STRONG
            record.normal = conf.NORMAL
            record.magenta = conf.MAGENTA
        else:
            record.levelcolor = ""
            record.strong = ""
            record.normal = ""
            record.magenta = ""

        if record.name == "abuild":
            record.prompt = ""
        else:
            if "." in record.name:
                record.prompt = record.name.split(".")[-1]
            else:
                record.prompt = record.name
            if record.prompt.endswith("*") and self.use_color:
                record.prompt = record.prompt.replace(
                    "*", f"{conf.BLUE}*{conf.NORMAL}")

        if record.prompt:
            record.prompt += ": "

        if record.levelname == "INFO":
            record.prettylevel = ">>> "
        elif record.levelno == 25:
            record.prettylevel = "\t"
            record.prompt = ""
        else:
            record.prettylevel = f">>> {record.levelname}: "

        return super().format(record)

def init_logger(name, output=sys.stderr, level="INFO", colors=False, time=False):
    """
    .. function:: init_logger(name, [output=sys.stderr, level="INFO", colors=False, time=False])

       Convenience function to initialize a :class:`logging.Logger` object and
       attach a :class:`logging.StreamHandler` to it pointed to ``output``,
       along with a :class:`.abuildLogFormatter` to format the messages.
       ``name`` is used for the logger name and ``level`` for the default
       logging threshold (i.e. only log messages with this log level or higher
       are handled). ``colors`` and ``time`` are passed to the
       :class:`.abuildLogFormatter`.

       :rtype: :class:`logging.Logger`
    """
    logger = logging.getLogger(name)
    logger.setLevel(level)
    handler = logging.StreamHandler(output)
    formatter = abuildLogFormatter(use_color=colors, show_time=time)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger

def conf_env_crosscompile():
    # UNDOC
    if (environ["CHOST"] != environ["CTARGET"]
            or environ["CBUILD"] != environ["CHOST"]):
        set_default(
            environ, "CBUILDROOT",
            str(Path("~/sysroot-" + environ["CTARGET_ARCH"] + "/").expanduser()))

    if environ["CBUILD"] != environ["CHOST"]:
        # prepare pkg-config for cross building
        set_default(
            environ, "PKG_CONFIG_PATH",
            environ["CBUILDROOT"] + "/usr/lib/pkgconfig/")
        set_default(environ, "PKG_CONFIG_SYSROOT_DIR", environ["CBUILDROOT"])
        # libtool bug workaround for extra rpaths
        set_default(
            environ, "lt_cv_sys_lib_dlsearch_path_spec",
            "{}/lib {}/usr/lib /usr/lib /lib /usr/local/lib".format(
                environ["CBUILDROOT"], environ["CBUILDROOT"]))

        if unset_or_null(environ, "CROSS_COMPILE"):
            environ["CROSS_COMPILE"] = environ["CHOST"] + "-"
            environ["HOSTCC"] = environ["CC"]
            environ["HOSTCXX"] = environ["CXX"]
            environ["HOSTLD"] = environ["LD"]
            environ["HOSTCPPFLAGS"] = environ["CPPFLAGS"]
            environ["HOSTCXXFLAGS"] = environ["CXXFLAGS"]
            environ["HOSTCFLAGS"] = environ["CFLAGS"]
            environ["HOSTLDFLAGS"] = environ["LDFLAGS"]
            environ["CC"] = environ["CROSS_COMPILE"] + "gcc"
            environ["CXX"] = environ["CROSS_COMPILE"] + "g++"
            environ["LD"] = environ["CROSS_COMPILE"] + "ld"
            environ["CPPFLAGS"] = (
                "--sysroot=" + environ["CBUILDROOT"] + " "
                + environ["CPPFLAGS"])
            environ["CXXFLAGS"] = (
                "--sysroot=" + environ["CBUILDROOT"] + " "
                + environ["CXXFLAGS"])
            environ["CFLAGS"] = (
                "--sysroot=" + environ["CBUILDROOT"] + " "
                + environ["CFLAGS"])
            environ["LDFLAGS"] = (
                "--sysroot=" + environ["CBUILDROOT"] + " "
                + environ["LDFLAGS"])

def conf_env_arches():
    # UNDOC
    set_default(
        environ, "CBUILD",
        child.get_stdout_strip(["gcc", "-dumpmachine"]))
    set_default(environ, "CHOST", environ["CBUILD"])
    set_default(environ, "CTARGET", environ["CHOST"])

    if arch_to_hostspec(environ["CBUILD"]) != "unknown":
        environ["CBUILD"] = arch_to_hostspec(environ["CBUILD"])
    if arch_to_hostspec(environ["CHOST"]) != "unknown":
        environ["CHOST"] = arch_to_hostspec(environ["CHOST"])
    if arch_to_hostspec(environ["CTARGET"]) != "unknown":
        environ["CTARGET"] = arch_to_hostspec(environ["CTARGET"])

    set_default(environ, "CARCH", hostspec_to_arch(environ["CBUILD"]))
    set_default(environ, "CLIBC", hostspec_to_libc(environ["CHOST"]))
    set_default(environ, "CBUILD_ARCH", hostspec_to_arch(environ["CBUILD"]))
    set_default(environ, "CTARGET_ARCH", hostspec_to_arch(environ["CTARGET"]))
    set_default(environ, "CTARGET_LIBC", hostspec_to_libc(environ["CTARGET"]))

def conf_env():
    # UNDOC

    args = [conf.SHELLEXPAND_PATH]
    # Figure out which configuration files to look at
    if "ABUILD_CONF" not in environ:
        environ["ABUILD_CONF"] = str(conf.ABUILD_CONF)
    if "ABUILD_USERDIR" not in environ:
        environ["ABUILD_USERDIR"] = str(conf.ABUILD_USERDIR.expanduser())
    if "ABUILD_USERCONF" not in environ:
        environ["ABUILD_USERCONF"] = str(conf.ABUILD_USERCONF.expanduser())

    if Path(environ["ABUILD_CONF"]).is_file():
        args.append(environ["ABUILD_CONF"])
    if not Path(environ["ABUILD_USERCONF"]).is_file():
        environ["ABUILD_USERCONF"] = str(
            Path(environ["ABUILD_USERDIR"]) / "abuild.conf")
        if not Path(environ["ABUILD_USERCONF"]).is_file():
            environ["ABUILD_USERCONF"] = ""
    if environ["ABUILD_USERCONF"]:
        args.append(environ["ABUILD_USERCONF"])

    if len(args) == 1:
        debug("No configuration files given...")
    else:
        res = child.get_stdout(args)
        if not res:
            debug("shellexpand returned no output?")
        else:
            for line in res.split("\0"):
                assignment = conf.RE_ASSIGNMENT.fullmatch(line)
                if assignment:
                    assignment = assignment.groups()
                    name = assignment[0]
                    value = assignment[1]

                    if name in environ:
                        continue
                    elif name != "POSIXLY_CORRECT":
                        environ[name] = value

    # Do not set_default because setting to null is like disabling
    if "FAKEROOT" not in environ:
        environ["FAKEROOT"] = "fakeroot"

    set_default(environ, "SUDO_APK", "abuild-apk")
    set_default(environ, "APK", "apk")
    set_default(environ, "ADDUSER", "abuild-adduser")
    set_default(environ, "ADDGROUP", "abuild-addgroup")
    set_default(environ, "CFLAGS", "")
    set_default(environ, "CXXFLAGS", "")
    set_default(environ, "GIT", child.get_stdout_strip(
        "command -v git || printf true",
        shell=True))
    set_default(environ, "PACKAGER", "Unknown")
    environ["GIT"] = shell_quote(environ["GIT"])

    conf_env_arches()
    conf_env_crosscompile()
