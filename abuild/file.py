# Copyright (c) 2018 Max Rees
# Distributed under GPL-2.0-only
# See LICENSE for more information.
"""
Module abuild.file
======================

.. module:: abuild.file
   :synopsis: Classes for representation of APKBUILD and PKGINFO files.
"""
import sys                 # stderr
from pathlib import Path
from os import environ
from pprint import PrettyPrinter
from collections import OrderedDict

import abuild.config as conf
import abuild.common as com
import abuild.child as child

class PKGINFO(OrderedDict):
    def __init__(self, filename):
        """
        .. class:: PKGINFO(filename)

           A subclass of :class:`collections.OrderedDict` that allows multiple
           keys of the same name by initializing nonexistent keys to an empty
           list, then appending values to that list. The ``filename`` parameter
           is used by :meth:`.PKGINFO.writeout`.

           >>> p = PKGINFO("pkg/.control-pkgname/.PKGINFO")
           >>> p["depend"] = "dep1"
           >>> p["depend"] = "dep2"
           >>> p["depend"] = "dep2"
           >>> p["depend"] = ["dep3", "dep4"]
           >>> p["depend"] = ["dep1", "dep5"]
           >>> p["depend"]
           ['dep1', 'dep2', 'dep3', 'dep4', 'dep5']
        """
        self.file = filename
        self.sequence = []
        super().__init__()

    def __setitem__(self, key, value):
        if key not in self:
            super().__setitem__(key, [])

        if isinstance(value, list):
            for subvalue in value:
                if subvalue in self[key]:
                    continue
                self[key].append(subvalue)
                self.sequence.append(f"{key} = {subvalue}")

        else:
            if value in self[key]:
                return

            self[key].append(value)
            if key.startswith("#"):
                self.sequence.append(f"{key}")
            else:
                self.sequence.append(f"{key} = {value}")

    def comment(self, comment):
        self["# " + comment] = ""

    def writeout(self, mode="w"):
        """
        .. method:: PKGINFO.writeout([mode="w"])

           Open the associated file with the given ``mode`` and write the
           ``{key} = {value}`` pairs to the file. An entry is written for each
           value string in the ``PKGINFO[key]`` list.
        """
        if not self:
            return

        with open(self.file, mode) as f:
            for line in self.sequence:
                f.write(f"{line}\n")

class APKBUILD:
    # pylint: disable=dangerous-default-value
    def __init__(self, filename, env=environ):
        """
        .. class:: APKBUILD(filename, [env=environ])

           Create a new APKBUILD representation object. ``filename`` is the
           path to the file itself. ``env`` is the environment with which the
           APKBUILD is inspected. The most important key of ``env`` is
           :envvar:`srcdir`, which specifies the location of the symlinks to
           the ``source`` files. The default, if left unset or null, is to use
           the parent directory of ``filename``. This is needed to properly
           expand ``builddir`` if it has a non-default value in the APKBUILD.
        """
        self.file = Path(filename).resolve()

        self.env = env
        if com.unset_or_null(self.env, "srcdir"):
            self.env["srcdir"] = str(self.file.parent.resolve())
        if com.unset_or_null(self.env, "APKBUILD"):
            self.env["APKBUILD"] = str(self.file)

        #: .. attribute:: APKBUILD.meta
        #:
        #:    This dictionary contains most of the properties of the APKBUILD.
        #:    It is typically not used directly because of
        #:    :meth:`.APKBUILD.__getattr__`.
        self.meta = {}

        #: .. attribute:: APKBUILD.expected_funcs
        #:
        #:    This is a set of shell function names that, based on the contents
        #:    of ``subpackages``, are expected to be present in the APKBUILD
        #:    file. By default it also contains ``package()``.
        self.expected_funcs = {"package",}

        #: .. attribute:: APKBUILD.funcs
        #:
        #:    This is the set of shell functions that are actually present in
        #:    the APKBUILD file.
        self.funcs = {}

        self.msgname = "abuild." + self.file.parent.name
        if not com.unset_or_null(self.env, "FAKEROOTKEY"):
            self.msgname += "*"

        #: .. todo::
        #:
        #:    Should :meth:`.APKBUILD.__init__` call :meth:`.APKBUILD.expand`,
        #:    :meth:`.APKBUILD.calc_subpkg_funcs`, and :meth:`.APKBUILD.parse`?
        self.expand()
        self.calc_subpkg_funcs()
        self.parse()
        self.sha256 = self.calc_sha256()

    def __getattr__(self, name):
        """
        .. method:: APKBUILD.__getattr__(name)

           Syntactic sugar to allow the usage of ``APKBUILD.name`` instead of
           ``APKBUILD.meta["name"]``.
        """
        return self.meta[name]

    def warning(self, *args, **kwargs):
        """
        .. method:: APKBUILD.warning(*args, **kwargs)
        """
        com.warning(*args, name=self.msgname, **kwargs)

    def die(self, *args, **kwargs):
        """
        .. method:: APKBUILD.die(*args, **kwargs)

           Convenience methods for the logging functions from
           :mod:`.abuild.common` that set the logger name to ``abuild.`` plus
           the main package name.
        """
        com.die(*args, name=self.msgname, **kwargs)

    def _expand(self):
        """
        .. method:: APKBUILD._expand()

           Internal function to execute the shell process that will return the
           raw expanded APKBUILD file.
        """
        res = child.get_stdout([conf.SHELLEXPAND_PATH, self.file], env=self.env)

        if not res:
            com.debug("shellexpand returned no output?")
            return ""

        return res

    def expand(self):
        """
        .. method:: APKBUILD.expand()

           Inspect the APKBUILD and fill the :attr:`.APKBUILD.meta` dictionary
           with the APKBUILD's properties. This function performs a basic
           sanity check by making sure that each of
           :data:`.REQUIRED_PROPERTIES` is present in the APKBUILD. It will
           then set the rest of :data:`.GENERAL_PROPERTIES` and
           :data:`.SPLITTABLE_PROPERTIES` to null values.
        """

        for line in self._expand().split("\0"):
            line = line.replace("\t", " ")
            # Store variable assignments into either metadata or env
            assignment = conf.RE_ASSIGNMENT.fullmatch(line)
            line = line.strip()
            if assignment:
                assignment = assignment.groups()
                name = assignment[0]
                value = assignment[1]

                if name in conf.GENERAL_PROPERTIES:
                    self.meta[name] = value
                elif name in conf.SPLITTABLE_PROPERTIES:
                    self.meta[name] = value.split()

                    if "depends" in name:
                        self.meta[name] = set(self.meta[name])

                elif name == "subpackages":
                    self.meta[name] = []
                    i = 0
                    for subpkg in value.split():
                        subpkg = subpkg.split(":")
                        if subpkg[0].endswith("-dbg"):
                            if i != 0:
                                self.warning(
                                    "dbg subpackage wasn't listed first in"
                                    " $subpackages, moving it for you")

                            self.meta[name].insert(0, subpkg)
                        else:
                            self.meta[name].append(subpkg)

                        i += 1

                elif name in conf.CHECKSUMS_READ:
                    self.meta[name] = value.splitlines()

        for prop in conf.REQUIRED_PROPERTIES:
            if prop not in self.meta:
                com.error("missing property '{}'".format(prop))

        for prop in conf.GENERAL_PROPERTIES:
            if prop not in self.meta:
                self.meta[prop] = ""

        for prop in conf.SPLITTABLE_PROPERTIES:
            if prop not in self.meta:
                if "depends" in prop:
                    self.meta[prop] = set()
                else:
                    self.meta[prop] = []

        if "subpackages" not in self.meta:
            self.meta["subpackages"] = []

        if "builddir" in self.meta:
            self.env["builddir"] = self.meta["builddir"]

    def dump_info(self):
        """
        .. method:: APKBUILD.dump_info()

           This is a convenience function that will pretty print
           :attr:`.APKBUILD.meta` to directly to ``stdout``.
        """
        pp = PrettyPrinter()
        pp.pprint(self.meta)

    def calc_subpkg_funcs(self):
        """
        .. method:: APKBUILD.calc_subpkg_funcs()

           This method populates the :attr:`.APKBUILD.expected_funcs` attribute
           with a function name for each of the subpackages listed in
           `subpackages`. If the subpackage was written in the APKBUILD in
           either the form ``pkgname-sub:splitfunc`` or
           ``pkgname-sub:splitfunc:arch`` (but not ``pkgname-sub::arch``), the
           name will be ``splitfunc``.  Otherwise, it will be ``sub`` (i.e the
           segment after the final dash).
        """
        if "subpackages" in self.meta:
            for subpkg in self.subpackages:
                if len(subpkg) > 1 and subpkg[1]:
                    # Add the splitfunc if the subpackage is of the form
                    # subpkgname:splitfunc[:arch]
                    self.expected_funcs.add(subpkg[1])
                else:
                    # Add the part after the last dash
                    i = subpkg[0].rfind("-")
                    splitfunc = subpkg[0][i + 1 :]
                    if len(subpkg) > 1:
                        subpkg[1] = splitfunc
                    else:
                        subpkg.append(splitfunc)
                    self.expected_funcs.add(splitfunc)

    def parse(self):
        """
        .. method:: APKBUILD.parse()

           Open the APKBUILD file directly and parse for ``Contributor`` lines,
           ``Maintainer`` lines, or shell functions. If
           :data:`.CHECK_REQUIRED` is True, ``!check`` is not in ``options``,
           and there is no ``check()`` function in the APKBUILD, an exception
           will be thrown. Additionally, if any of the functions in
           :attr:`.APKBUILD.expected_funcs` are missing, and those functions
           have no default (as defined by :data:`.FUNCS_WITH_DEFAULT`), an
           exception will be thrown.
        """
        funcname = ""
        in_func = 0

        with open(self.file, "r") as f:
            for line in f.readlines():
                funchead = conf.RE_FUNCHEAD.fullmatch(line.rstrip("\n"))
                functail = conf.RE_FUNCTAIL.fullmatch(line.rstrip("\n"))
                funcinline = conf.RE_FUNCINLINE.fullmatch(line.rstrip("\n"))
                emails = conf.RE_EMAILS.fullmatch(line.rstrip("\n"))
                if emails:
                    emails = emails.groups()
                    if len(emails) > 1 and emails[1]:
                        email_type = emails[0].lower()
                        if email_type in self.meta:
                            self.meta[email_type].append(emails[1].strip())
                        else:
                            self.meta[email_type] = [emails[1].strip()]

                elif funchead:
                    funcname = funchead.groups()[0]
                    self.funcs[funcname] = ""
                    in_func = 1

                elif functail:
                    in_func = 0

                elif in_func:
                    self.funcs[funcname] += line

                elif funcinline:
                    funcinline = funcinline.groups()
                    self.funcs[funcinline[0]] = funcinline[1]

        if "check" not in self.funcs and "!check" not in self.options:
            if conf.CHECK_REQUIRED:
                self.die("Missing check() function in APKBUILD")

        for func in self.expected_funcs:
            if func not in self.funcs and func not in conf.FUNCS_WITH_DEFAULT:
                self.die("Missing {}() function in APKBUILD".format(func))

        if "maintainer" not in self.meta:
            self.meta["maintainer"] = []
        if "contributor" not in self.meta:
            self.meta["contributor"] = []

    def calc_sha256(self):
        return child.pipeline(
            # Strip all leading / trailing whitespace, comments, blanks
            ["sed", "-e", r"s/^[ \t]*//g; s/[ \t]*$//g; /^#/d; /^$/d", self.file],
            ["sha256sum", "-"],
            ["cut", "-d", " ", "-f", "1"],
            last_stdout="capture").strip()

if __name__ == "__main__":
    if len(sys.argv) > 1:
        for arg in sys.argv[1:]:
            a = APKBUILD(arg)
            a.dump_info()
