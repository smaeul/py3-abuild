# Copyright (c) 2018 Max Rees
# Distributed under GPL-2.0-only
# See LICENSE for more information.
import os         # pipe, set_inheritable, close
import subprocess # Popen, PIPE, run
import sys        # exit
import logging

def get_stdout(*args, **kwargs):
    return run_child(*args, output="stdout", **kwargs)[1]

def get_stdout_strip(*args, **kwargs):
    return run_child(*args, output="stdout", **kwargs)[1].strip("\n")

def get_retcode(*args, **kwargs):
    return run_child(*args, **kwargs)[0]

def debug(*args, **kwargs):
    logging.getLogger("abuild").debug(*args, **kwargs)

def die(*args, **kwargs):
    logging.getLogger("abuild").error(*args, **kwargs)
    sys.exit(1)

def run_child(*args, output=False, retcodes=[], **kwargs):
    # UNDOC
    # check_output for subprocess.run is only available in 3.7
    if output == "both":
        stdout = subprocess.PIPE
        stderr = subprocess.PIPE
    elif output == "stdout":
        stdout = subprocess.PIPE
        stderr = None
    elif output == "stderr":
        stdout = None
        stderr = subprocess.PIPE
    else:
        stdout = None
        stderr = None

    if isinstance(args[0], list):
        cmd = " ".join(str(arg) for arg in args[0])
    else:
        cmd = args[0]
    debug("Running command: " + cmd)
    try:
        res = subprocess.run(
            *args, stdout=stdout, stderr=stderr, encoding='utf-8', **kwargs)
    except KeyboardInterrupt:
        die("Interrupted by user")
    except FileNotFoundError:
        if isinstance(args[0], list):
            die("Could not find the executable: %s", args[0][0])
        else:
            die("Could not find the executable: %s", args[0])

    if res.returncode != 0:
        if retcodes != "all" and res.returncode not in retcodes:
            die("Child exited with status {}".format(res.returncode))

    if not hasattr(res, "stdout") or not res.stdout:
        res.stdout = ""

    if not hasattr(res, "stderr") or not res.stderr:
        res.stderr = ""

    return (res.returncode, res.stdout, res.stderr)


def new_child_pipe():
    pipe = os.pipe()
    os.set_inheritable(pipe[0], True)
    os.set_inheritable(pipe[1], True)
    return pipe

def pipeline(*commands, first_stdin=None, last_stdout=None):
    """
    .. function:: pipeline(*commands, [first_stdin=None, last_stdout=None])

       Run a series of commands as a pipeline. The first command will read from
       the file ``first_stdin``, if given. The last command will write to
       ``last_stdout``, if given (otherwise to the controlling terminal's
       stdout).
    """
    total = len(commands)
    if total < 2 and not last_stdout:
        raise NotImplementedError
    if last_stdout == "capture":
        last_stdout = subprocess.PIPE
        encoding = "utf-8"
    else:
        encoding = None
    processes = []
    pipes = []
    i = 0

    for cmd in commands:
        if i == 0:
            pipes.append(new_child_pipe())
            stdin = first_stdin
            if total == 1 and last_stdout:
                stdout = last_stdout
            else:
                stdout = pipes[i][1]
        elif 0 < i < total - 1:
            pipes.append(new_child_pipe())
            stdin = pipes[i - 1][0]
            stdout = pipes[i][1]
        elif i == total - 1:
            stdin = pipes[i - 1][0]
            stdout = last_stdout

        process = subprocess.Popen(
            cmd, stdin=stdin, stdout=stdout, encoding=encoding)
        processes.append(process)
        i += 1

    i = 0
    for process in processes:
        process.wait()
        if i < total - 1:
            os.close(pipes[i][0])
            os.close(pipes[i][1])
        i += 1

    if last_stdout == subprocess.PIPE:
        return processes[-1].stdout.read()

    return ""
