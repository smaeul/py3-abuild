# Copyright (c) 2018 Max Rees
# Distributed under GPL-2.0-only
# See LICENSE for more information.
import os     # chdir, rmdir
import shutil # rmtree
import shlex  # quote
from pathlib import Path
from urllib.request import urlretrieve

import abuild.config as conf
import abuild.common as com
import abuild.child as child

def remote_fetch(src, srcdest):
    """
    .. function:: remote_fetch(src, srcdest)

       Fetch a remote source.

       :param str src:
          The source path or URI. This can be of the form
          "https://example.com/example.tar.gz" or
          "example-1.0-r0.tar.gz::https://example.com/example.tar.gz".
       :param str srcdest:
          The location where the file should be saved (usually is
          ``Abuild.env["SRCDEST"]``).
    """
    name = com.filename_from_uri(src)
    url = com.get_remote_uri(src)
    urlretrieve(url, srcdest / name)

def clean(self):
    """
    .. method:: Abuild.clean()

       Recursively remove $srcdir and $pkgbasedir
    """
    self.msg("Cleaning temporary build dirs...")
    shutil.rmtree(self.env["srcdir"], ignore_errors=True)
    shutil.rmtree(self.env["pkgbasedir"], ignore_errors=True)
    try:
        os.rmdir(self.env["ABUILD_TMPDIR"])
    except OSError:
        pass

def fetch(self):
    """
    .. method:: Abuild.fetch()

       Fetch all remote sources and create the symlinks in $srcdir
    """
    self.env["srcdir"].mkdir(exist_ok=True, parents=True)

    for src in self.source:
        if com.is_remote(src):
            self.msg("Fetching", src, "...")
            remote_fetch(src, self.env["SRCDEST"])
            name = com.filename_from_uri(src)
            target = self.env["SRCDEST"] / name
            link_name = self.env["srcdir"] / name
        else:
            target = self.env["startdir"] / src
            link_name = self.env["srcdir"] / src

        com.force_symlink(target, link_name)

def verify(self):
    """
    .. method:: Abuild.verify()

       Verify all checksums in the APKBUILD

       :data:`.CHECKSUMS_READ` determines which checksum properties are
       examined.
    """
    self.fetch()

    # Note: -c (--check) is the only portable option between coreutils
    # and busybox
    args = ["false", "-c", "-"]
    os.chdir(self.env["srcdir"])

    for algo in conf.CHECKSUMS_READ:
        args[0] = algo.rstrip("s")
        if com.unset_or_null(self.APKBUILD.meta, algo):
            continue

        csums = self.APKBUILD.meta[algo]
        if len(csums) != len(self.source):
            self.die(
                f"Number of {algo} ({len(csums)}) !="
                f" number of sources ({len(self.source)})")

        csums_dict = {}
        for csum in csums:
            name = csum.split(" ", maxsplit=1)[1].strip()
            csums_dict[name] = csum.strip()

        res = True
        for src in self.source:
            name = com.filename_from_uri(src)
            if not name in csums_dict:
                self.error(f"{name} missing from {algo}")
                res = False
                continue

            csum = csums_dict[name]
            if child.get_retcode(args, input=csum, retcodes=[1]) == 1:
                res = False

                if not com.is_remote(src):
                    continue

                else:
                    csum = csum[0:8]
                    path = Path(self.env["SRCDEST"])
                    path_new = path / (name + "." + csum)
                    path = path / name
                    self.error(
                        f"Because the remote file above failed the {algo}"
                        " check it will be renamed.")
                    self.error(f"Renaming {path.name} to {path_new.name}")
                    path.rename(path_new)

    os.chdir(self.env["startdir"])
    if not res:
        raise com.abuild_error()

def unpack(self):
    """
    .. method:: Abuild.unpack()

       Unpack any archives listed in $source into $builddir

       .. todo:: Add initdcheck
    """
    self.verify()
    self.env["srcdir"].mkdir(exist_ok=True, parents=True)

    gunzip = child.get_stdout_strip(
        "command -v pigz || printf gunzip", shell=True)
    if gunzip.endswith("pigz"):
        gunzip += " -d"
    srcdir = str(self.env["srcdir"])
    srcdir_s = shlex.quote(srcdir)

    for src in self.source:
        shell = False
        if com.is_remote(src):
            path = Path(self.env["SRCDEST"]) / com.filename_from_uri(src)
        else:
            path = self.env["startdir"] / src
        path_s = shlex.quote(str(path))

        if path.match("*.tar"):
            args = ["tar", "-C", srcdir, "-xf", str(path)]
        elif path.match("*.tar.gz") or path.match("*.tgz"):
            shell = True
            args = f"{gunzip} -c {path_s} | tar -C {srcdir_s} -f - -x"
        elif path.match("*.tar.bz2"):
            args = ["tar", "-C", srcdir, "-jxf", str(path)]
        elif path.match("*.tar.xz"):
            shell = True
            args = f"unxz --threads=0 -c {path_s} | tar -C {srcdir_s} -f - -x"
        elif path.match("*.zip"):
            args = ["unzip", "-n", "-q", str(path), "-d", srcdir]
        elif path.match("*.tar.lz"):
            args = ["tar", "-C", srcdir, "--lzip", "-xf", str(path)]
        elif path.match("*.tar.lzma"):
            shell = True
            args = f"unlzma -T 0 -c {path_s} | tar -C {srcdir_s} -f - -x"
        else:
            args = []

        if args:
            self.msg("Unpacking", path, "...")
            child.get_retcode(args, shell=shell)
