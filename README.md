py3-abuild
==========
This is a **work-in-progress** rewrite of the
[``abuild``](https://git.alpinelinux.org/cgit/abuild/) build system for Alpine
Package Keeper-based Linux operating system distributions. It intends to also
provide a library from which other applications can be built.

Usage
=====
The package comes with a command line interface currently called ``pbuild``.
It is intended to be a mostly-compatible replacement for ``abuild``.

```sh
$ python3 setup.py install
$ pbuild --help
```

See the [``TODO``](TODO.md) file for more information.

Licensing
=========
* ``abuild/digraph.py``: [``MIT``](LICENSE.dag)
* Everything else: ``GPL-2.0-only``
