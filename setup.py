#!/usr/bin/env python3
# Copyright © 2018 Samuel Holland
# Distributed under GPL-2.0-only
# See LICENSE for more information.

from distutils.core import setup
from glob import glob

setup(
    name='py3-abuild',
    packages=['abuild'],
    scripts=['pbuild'],
    data_files=[('libexec/abuild', glob("helpers/*.sh"))]
)
