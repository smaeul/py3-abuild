PYTHON        = python3
DOC           = doc
CHECKTARGETS  = abuild/file.py abuild/common.py
DOCTARGETS    = $(DOC)/config.rst $(DOC)/common.rst $(DOC)/__init__.rst $(DOC)/file.rst

all:

# Documentation
SPHINXOPTS    =
SPHINXBUILD   = sphinx-build-3
SPHINXPROJ    = py3-abuild
DOCBUILD      = $(DOC)/_build
$(DOC): $(DOCTARGETS)
	@$(SPHINXBUILD) -M html "$(DOC)" "$(DOCBUILD)" $(SPHINXOPTS) $(O)
$(DOC)/%.rst: abuild/%.py
	"$(DOC)/mkdocs.py" < "$<" > "$@"

# Tests
check: $(CHECKTARGETS)
	$(PYTHON) -m doctest -v $(CHECKTARGETS)
	$(PYTHON) tests/run_tests.py
