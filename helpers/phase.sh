#!/bin/sh -e
# Copyright (c) 2018 Max Rees
# Distributed under GPL-2.0-only
# See LICENSE for more information.

datadir="/usr/share/abuild"
# This will cause coreutils to do funky things and tests will fail
unset POSIXLY_CORRECT

if ! printf '%s' "$1" | grep -q '^/'; then
  . "./$1"
else
  . "$1"
fi
shift

for var in $EXPORT_VARS; do
  export $var
done
if [ "$CHOST" != "$CTARGET" ]; then
  export CBUILDROOT
elif [ "$CBUILD" != "$CHOST" ]; then
  export CBUILDROOT PKG_CONFIG_PATH PKG_CONFIG_SYSROOT_DIR
  export lt_cv_sys_lib_dlsearch_path_spec
  export CROSS_COMPILE HOSTCC HOSTCXX HOSTLD HOSTCPPFLAGS HOSTCXXFLAGS
  export HOSTCFLAGS HOSTLDFLAGS CC CXX LD CPPFLAGS CXXFLAGS CFLAGS LDFLAGS
fi

if [ -n "$USE_COLORS" ]; then
  NORMAL="\033[1;0m"
  STRONG="\033[1;1m"
  RED="\033[1;31m"
  GREEN="\033[1;32m"
  YELLOW="\033[1;33m"
  BLUE="\033[1;34m"
fi

msg() {
	[ -n "$quiet" ] && return 0
	local prompt="$GREEN>>>${NORMAL}"
	local fake="${FAKEROOTKEY:+${BLUE}*${NORMAL}}"
	local name="${STRONG}${subpkgname:-$pkgname}${NORMAL}"
	printf "${prompt} ${name}${fake}: %s\n" "$1" >&2
}

warning() {
	local prompt="${YELLOW}>>> WARNING:${NORMAL}"
	local fake="${FAKEROOTKEY:+${BLUE}*${NORMAL}}"
	local name="${STRONG}${subpkgname:-$pkgname}${NORMAL}"
	printf "${prompt} ${name}${fake}: %s\n" "$1" >&2
}

error() {
	local prompt="${RED}>>> ERROR:${NORMAL}"
	local fake="${FAKEROOTKEY:+${BLUE}*${NORMAL}}"
	local name="${STRONG}${subpkgname:-$pkgname}${NORMAL}"
	printf "${prompt} ${name}${fake}: %s\n" "$1" >&2
}

die() {
	error "$*"
	exit 1
}

if [ -z "$ABUILD" ]; then
  warning '$ABUILD is unset, so "default_*" directives will fail'
  ABUILD='die $ABUILD is unset, so cannot perform: '
fi

initdcheck() {
	local i line
	for i in $source; do
		case $i in
		*.initd) line=$(head -n 1 "$srcdir"/$i);;
		*) continue ;;
		esac

		case "$line" in
		*sbin/openrc-run)
			;;
		*sbin/runscript)
			warning "$i is not an openrc #!/sbin/openrc-run"
			;;
		*)	error "$i is not an openrc #!/sbin/openrc-run"
			return 1
			;;
		esac
	done
}

default_fetch() {
  cd "$startdir"
  $ABUILD $ABUILD_OPTS default_fetch
  cd "$OLDPWD"
}

default_unpack() {
  cd "$startdir"
  $ABUILD $ABUILD_OPTS default_unpack
  cd "$OLDPWD"
}

default_prepare() {
  cd "$startdir"
  $ABUILD $ABUILD_OPTS default_prepare
  cd "$OLDPWD"
}

update_config_sub() {
	find . -name config.sub | (local changed=false; while read f; do
		if ! ./$f armv6-alpine-linux-muslgnueabihf 2>/dev/null; then
			msg "Updating $f"
			cp "$datadir"/${f##*/} "$f" || return 1
			changed=true
		else
			msg "No update needed for $f"
		fi
	done; $changed)
}

update_config_guess() {
	find . -name config.guess | (local changed=false; while read f; do
		if grep -q aarch64 "$f" && grep -q ppc64le "$f"; then
			msg "No update needed for $f"
		else
			msg "Updating $f"
			cp "$datadir"/${f##*/} "$f" || return 1
			changed=true
		fi
	done; $changed)
}

default_build() {
  :
}

default_dbg() {
	local f
	pkgdesc="$pkgdesc (debug symbols)"

	binfiles=$(scanelf -R "$pkgdir" | grep ET_DYN | sed "s:$pkgdir\/::g" | sed "s:ET_DYN ::g")
	for f in $binfiles; do
		srcdir=$(dirname $pkgdir/$f)
		srcfile=$(basename $pkgdir/$f)
		dstdir=$(dirname $subpkgdir/usr/lib/debug/$f.debug)
		dstfile=$(basename $subpkgdir/usr/lib/debug/$f.debug)
		if [ ! -d $dstdir ] ; then
			mkdir -p $dstdir
		fi
		cd $srcdir
		local XATTR=$(getfattr --match="" --dump "${srcfile}")
		${CROSS_COMPILE}objcopy --only-keep-debug $srcfile $dstfile
		${CROSS_COMPILE}objcopy --add-gnu-debuglink=$dstfile $srcdir/$srcfile
		mv $dstfile $dstdir
		${CROSS_COMPILE}strip $srcfile
		[ -n "$XATTR" ] && { echo "$XATTR" | setfattr --restore=-; }
	done
	return 0
}

default_dev() {
	local i= j=
	depends="$depends_dev"
	pkgdesc="$pkgdesc (development files)"

	cd "$pkgdir" || return 0
	local libdirs=usr/
	[ -d lib/ ] && libdirs="lib/ $libdirs"
	for i in usr/include usr/lib/pkgconfig usr/share/aclocal\
			usr/share/gettext usr/bin/*-config	\
			usr/share/vala/vapi usr/share/gir-[0-9]*\
			usr/share/qt*/mkspecs			\
			usr/lib/qt*/mkspecs			\
			usr/lib/cmake				\
			$(find . -name include -type d) 	\
			$(find $libdirs -name '*.[acho]' \
				-o -name '*.prl' 2>/dev/null); do
		if [ -e "$pkgdir/$i" ] || [ -L "$pkgdir/$i" ]; then
			d="$subpkgdir/${i%/*}"	# dirname $i
			mkdir -p "$d"
			mv "$pkgdir/$i" "$d"
			rmdir "$pkgdir/${i%/*}" 2>/dev/null || :
		fi
	done
	# move *.so links needed when linking the apps to -dev packages
	for i in lib/*.so usr/lib/*.so; do
		if [ -L "$i" ]; then
			mkdir -p "$subpkgdir"/"${i%/*}"
			mv "$i" "$subpkgdir/$i" || return 1
		fi
	done
	return 0
}

default_doc() {
	depends="$depends_doc"
	pkgdesc="$pkgdesc (documentation)"
	install_if="docs $pkgname=$pkgver-r$pkgrel"

	local i
	for i in doc man info html sgml licenses gtk-doc ri help; do
		if [ -d "$pkgdir/usr/share/$i" ]; then
			mkdir -p "$subpkgdir/usr/share"
			mv "$pkgdir/usr/share/$i" "$subpkgdir/usr/share/"
		fi
	done

	# compress man pages
	local mandir="$subpkgdir"/usr/share/man
	[ -d "$mandir" ] && find "$mandir" -type l \
		-a \( -name \*.[0-8n] -o -name \*.[0-8][a-z]* \) \
		-a \! \( -name '*.gz' -o -name '*.bz2' -o -name '*.xz' \) \
		| while read symlink; do

		ln -s $(readlink $symlink).gz "$symlink".gz
		rm -f "$symlink"
	done
	[ -d "$mandir" ] && find "$mandir" -type f \
		-a \( -name \*.[0-8n] -o -name \*.[0-8][a-z]* \) \
		-a \! \( -name '*.gz' -o -name '*.bz2' -o -name '*.xz' \) \
		-exec stat -c "%i %n" \{\} \+ | while read inode name; do

		# Skip hardlinks removed in last iteration.
		[ -f "$name" ] || continue

		local islink=0
		find "$mandir" -type f -links +1 \
			-a \( -name \*.[0-8n] -o -name \*.[0-8][a-z]* \) \
			-a \! \( -name '*.gz' -o -name '*.bz2' -o -name '*.xz' \) \
			-exec stat -c "%i %n" \{\} \+ | while read linode lname; do
			if [ "$linode" = "$inode" -a "$lname" != "$name" ]; then
				islink=1
				rm -f "$lname"
				ln -s "${name##*/}".gz "$lname".gz
			fi
		done

		[ $islink -eq 0 ] && gzip -9 "$name"
	done

	rm -f "$subpkgdir/usr/share/info/dir"

	# remove if empty, ignore error (not empty)
	rmdir "$pkgdir/usr/share" "$pkgdir/usr" 2>/dev/null || :
}

default_libs() {
	pkgdesc="$pkgdesc (libraries)"
	local dir= file=
	for dir in lib usr/lib; do
		for file in "$pkgdir"/$dir/lib*.so.[0-9]*; do
			[ -f "$file" ] || continue
			mkdir -p "$subpkgdir"/$dir
			mv "$file" "$subpkgdir"/$dir/
		done
	done
}

default_openrc() {
	pkgdesc="$pkgdesc (OpenRC init scripts)"
	install_if="openrc $pkgname=$pkgver-r$pkgrel"
	local dir file
	for dir in conf.d init.d; do
		if [ -d "$pkgdir/etc/$dir" ]; then
			mkdir -p "$subpkgdir"/etc
			mv "$pkgdir/etc/$dir" "$subpkgdir"/etc/
		fi
	done
	return 0
}

default_lang() {
	pkgdesc="Languages for package $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel lang"

	local dir
	for dir in ${langdir:-/usr/share/locale}; do
		mkdir -p "$subpkgdir"/${dir%/*}
		mv "$pkgdir"/"$dir" "$subpkgdir"/"$dir" || return 1
	done
}

"$1"
if [ -n "$2" ]; then
  env -0 > "$2"
fi
