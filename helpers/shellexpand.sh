#!/bin/sh -ea
# Copyright (c) 2018 Max Rees
# Distributed under GPL-2.0-only
# See LICENSE for more information.
while [ -n "$1" ]; do
  if ! printf '%s' "$1" | grep -q '^/'; then
    . "./$1"
  else
    . "$1"
  fi
  shift
done
exec env -0
