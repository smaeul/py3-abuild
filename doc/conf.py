#!/usr/bin/env python3
project = "py3-abuild"
copyright = "2018, Max Rees"
author = "Max Rees"
primary_domain = "py"

version = "0.0.1"
release = "0.0.1"

extensions = [
    "sphinx.ext.doctest",
    "sphinx.ext.todo",
    "sphinx.ext.coverage",
    "sphinx.ext.intersphinx",
]
templates_path = ["_templates"]
source_suffix = ".rst"
master_doc = "index"
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

language = None

todo_include_todos = True
todo_emit_warnings = False
todo_link_only = True

intersphinx_mapping = {"https://docs.python.org/3.6/": None,}

pygments_style = "sphinx"
html_theme = "classic"
# html_theme_options = {}
html_static_path = ["_static"]
htmlhelp_basename = "py3-abuilddoc"
