#!/usr/bin/env python3
# Copyright (c) 2018 Max Rees
# Distributed under GPL-2.0-only
# See LICENSE for more information.
# pylint: disable=invalid-name
import re
import fileinput

pattern_insert = re.compile(r"^\s*#: ?(\s*):INSERT: ?(.*)$")
pattern_comment = re.compile(r"^\s*#: ?")
pattern_docstring = re.compile(r"^(\s*)\"\"\"$")

insert = False
docstring = False
indent = ""

for line in fileinput.input():
    line = line.strip("\n")
    res = pattern_insert.match(line)
    if res:
        indent = res.group(1)
        lang = res.group(2) or "python"

        # Prepend code block identifier
        if not insert:
            print(indent + ".. code-block::", lang + "\n\n", end="")

        insert = not insert
        continue
    if insert:
        # Prepend indentation
        print(indent + "   ", end="")
        print(line)
        continue

    res = pattern_comment.match(line)
    if res:
        # Get all of the #: comments
        line = pattern_comment.sub("", line, count=1)
        print(line)
        continue

    res = pattern_docstring.match(line)
    if res:
        # Add a trailing newline
        if docstring:
            print()

        docstring = not docstring
        indent = res.group(1)
        continue
    if docstring:
        # Remove leading indentation that matches the """
        line = line.replace(indent, "", 1)
        print(line)
