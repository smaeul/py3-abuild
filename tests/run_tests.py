#!/usr/bin/env python3
import glob    # iglob
import sys     # argv
import pathlib # Path
import os      # chdir, environ
import re      # compile
import logging # getlogger
import shutil  # rmtree

testdir = pathlib.Path(os.getcwd())
if (testdir / "tests").is_dir():
    testdir /= "tests"
    os.chdir(testdir)
parent = testdir.parent.absolute()
os.environ["PATH"] = f"{parent}:{os.environ['PATH']}"
os.environ["PYTHONPATH"] = str(parent)
sys.path.append(os.environ["PYTHONPATH"])

import abuild.common as com
import abuild.child as child

def msg(*args, name="abuild-tester", **kwargs):
    com.msg(*args, name=name, **kwargs)

def msg2(*args, name="abuild-tester", **kwargs):
    com.msg2(*args, name=name, **kwargs)

def error(*args, name="abuild-tester", **kwargs):
    com.error(*args, name=name, **kwargs)

def die(*args, name="abuild-tester", **kwargs):
    try:
        com.die(*args, name=name, **kwargs)
    except com.abuild_error:
        sys.exit(1)

def save_output(stdout, stderr):
    pathlib.Path("stdout.log").write_text(stdout)
    pathlib.Path("stderr.log").write_text(stderr)

def check_patterns(output, patterns):
    if not patterns:
        return

    for line in output.splitlines():
        for pattern in patterns:
            if pattern.match(line):
                patterns.remove(pattern)

def fail_patterns(name, logname, patterns):
    if not patterns:
        return False

    error(f"Failed {name} output matches:", name=logname)
    for pattern in patterns:
        msg2(repr(pattern.pattern), name=logname)

    return True

if not (testdir / "run_tests.py").is_file():
    die("Could not find tests!")

com.init_logger("abuild-tester", colors=True)

repodest = testdir / "apks"
shutil.rmtree(repodest, ignore_errors=True)
repodest.mkdir(mode=0o755, parents=True)
os.environ["APORTSDIR"] = str(testdir.resolve())
os.environ["REPODEST"] = str(repodest.resolve())

if len(sys.argv) == 1:
    tests = glob.iglob(str(testdir / "*" / "APKBUILD"))
else:
    tests = sys.argv[1:]

for test in tests:
    path = pathlib.Path(test).parent
    name = path.name
    logname = "abuild-tester." + name
    args = [parent / "pbuild", "-m"]
    stdout_re = []
    stderr_re = []
    retcodes = [0]

    com.msg("Testing...", name=logname)

    with open(test) as f:
        for line in f.readlines():
            line = line.strip()

            if line.startswith("#:args: "):
                args.append(line.replace("#:args: ", "").split())

            elif line.startswith("#:stdout: "):
                line = line.replace("#:stdout: ", "")
                stdout_re.append(re.compile(line))

            elif line.startswith("#:stderr: "):
                line = line.replace("#:stderr: ", "")
                stderr_re.append(re.compile(line))

            elif line.startswith("#:retcodes: "):
                line = line.replace("#:retcodes: ", "").split()
                retcodes = [int(i) for i in line]

    os.chdir(path)
    ret, stdout, stderr = child.run_child(args, output="both", retcodes="all")
    stdout = stdout.strip()
    stderr = stderr.strip()

    if ret not in retcodes:
        save_output(stdout, stderr)
        die(f"Child exited with status {ret}", name=logname)

    if not stderr:
        save_output(stdout, stderr)
        die("No output generated!", name=logname)

    check_patterns(stdout, stdout_re)
    check_patterns(stderr, stderr_re)

    failed = fail_patterns("stdout", logname, stdout_re)
    failed = fail_patterns("stderr", logname, stderr_re) or failed

    if failed:
        save_output(stdout, stderr)
        sys.exit(1)

    os.chdir(testdir)
