Big picture to-do list
======================
Things to do before first release:
- [ ] Finish ``rootpkg``
- [ ] Rewrite default subpackage functions in Python
- [ ] Rewrite environment configuration functions to merge ``dict``s
- [ ] The rest of the command line options
- [ ] Crosscompile support
- [ ] Documentation
- [ ] Designation of public and internal APIs

Future releases:
- [ ] ``rootbld``
- [ ] Write ``APKBUILD`` class as a full parser instead of shelling out
